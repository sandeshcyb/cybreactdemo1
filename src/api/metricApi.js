export const  metricApi =()=>{
    return [
        {
            metricName : 'current Booked Vs  LY current Booked (same day)',
            thisYear   : '2019',
            lastYear   : '2018',
            thisYearMetric : 13500,
            lastYearMetric : 13000,
            year           : '2018-2019'
        },
        {
            metricName : 'Expected Average Fare Vs  LY Obsered Average Fare',
            thisYear   : '2019',
            lastYear   : '2018',
            thisYearMetric : 6000,
            lastYearMetric : 5000,
            year           : '2018-2019'
        },
        {
            metricName : 'Expected Yield Vs  LY Obsered Yield',
            thisYear   : '2019',
            lastYear   : '2018',
            thisYearMetric : 1.56,
            lastYearMetric : 1.67,
            year           : '2018-2019'
        },
        {
            metricName : 'Expected Revenue Vs  LY Obsered Revenue',
            thisYear   : '2019',
            lastYear   : '2018',
            thisYearMetric : 60320000,
            lastYearMetric : 49200000,
            year           : '2018-2019'
        },
        {
            metricName : 'Constrined Forecast Vs  LY Net Bookings',
            thisYear   : '2019',
            lastYear   : '2018',
            thisYearMetric : 3200,
            lastYearMetric : 4900,
            year           : '2018-2019'
        },
        {
            metricName : 'Market Share Vs  LY Net Bookings',
            thisYear   : '2019',
            lastYear   : '2018',
            thisYearMetric : '34%',
            lastYearMetric : '27%',
            year           : '2018-2019'
        },

        {
            metricName : 'current Booked Vs  LY current Booked (same day)',
            thisYear   : '2019',
            lastYear   : '2018',
            thisYearMetric : 14900,
            lastYearMetric : 15700,
            year           : '2018-2019'

        },

        {
            metricName : 'Expected Average Fare Vs  LY Obsered Average Fare',
            thisYear   : '2018',
            lastYear   : '2017',
            thisYearMetric : 4000,
            lastYearMetric : 7000,
            year           : '2018-2019'
        },
        {
            metricName : 'Expected Yield Vs  LY Obsered Yield',
            thisYear   : '2018',
            lastYear   : '2017',
            thisYearMetric : 1.56,
            lastYearMetric : 1.67,
            year           : '2017-2018'   
        },
        {
            metricName : 'Expected Revenue Vs  LY Obsered Revenue',
            thisYear   : '2018',
            lastYear   : '2017',
            thisYearMetric : 70320000,
            lastYearMetric : 59200000,
            year           : '2017-2018'   
        },
        {
            metricName : 'Constrined Forecast Vs  LY Net Bookings',
            thisYear   : '2018',
            lastYear   : '2017',
            thisYearMetric : 3900,
            lastYearMetric : 3400,
            year           : '2017-2018'   
        },
        {
            metricName : 'Market Share Vs  LY Net Bookings',
            thisYear   : '2018',
            lastYear   : '2017',
            thisYearMetric : '30%',
            lastYearMetric : '31%',
            year           : '2017-2018'   
        },
        {
            metricName : 'Market Share Vs  LY Net Bookings',
            thisYear   : '2017',
            lastYear   : '2016',
            thisYearMetric : '36%',
            lastYearMetric : '33%',
            year           : '2016-2017'   
        }

    ]
}

 