import React, { Component } from "react";
import Header from "./common/Header";
import SubHeader from "./common/SubHeader";
import HighChart from "./dashboard/components/HighChart";
import MetricOverview from "./dashboard/components/MetricOverview";
import AirlinesList from "./dashboard/components/AirlinesList";

class App extends Component {
  state = {
    sendYear: ""
  };

  //To get selected year.1111
  getSelectedYear = year => {
    this.setState({
      sendYear: year
    });
  };

  render() {
    return (
      <div>
        <Header />
        <SubHeader getYear={this.getSelectedYear} />
        <HighChart />
        <MetricOverview sendYear={this.state.sendYear} />
        <AirlinesList />
      </div>
    );
  }
}

export default App;
