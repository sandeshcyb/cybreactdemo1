// ****************************************************************************
// ***************** Component : Main Header        ***************************
// ***************** Developer : Vikas Jagdale (03/05/2019)  ******************
// ****************************************************************************

import React,{Component} from 'react';
class Header extends Component{
    state={
      searchVal : '',
    }

    handleChange = (event) =>{
      const target = event.target;
      const name   = target.name;
      const value  = target.value;
      this.setState({
        [name] : value
      })
    }

// ********************* Get search value  ******************//
    getSearchValue =(event)=>{
      if(this.state.searchVal){
        alert("your searched value is ---> "+this.state.searchVal);
      }else{
        alert("Please search anything first");
      }
    }

    render(){
        return(
            <nav className="navbar navbar-inverse container-fluid header-wrap" role="navigation">
            <div className="container-fluid">
              <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                  <span className="icon-bar"></span>
                </button>
                <a className="navbar-brand pros-navbar-brand" href="/">
                    <img src="/images/pros-logo.svg" alt="logo" />
                </a>
              </div>
              <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <div className="col-lg-5"></div>
                <div className="pros-hsearch col-lg-4">
                    <input type="text" className="searchTerm col-lg-12" autoComplete="off" placeholder="Search..." value={this.setState.searchVal} name="searchVal" onChange={this.handleChange}/>
                    <i className="fa fa-search PSIcon" onClick={this.getSearchValue}></i>
                </div>
                <div className="pros-media-icon col-lg-1 row">
                    <div className="pros-icons col-lg-4">
                        <i className="fa fa-repeat" aria-hidden="true"></i>
                    </div>
                    <div className="pros-icons col-lg-4">
                        <i className="fa fa-question-circle" aria-hidden="true"></i>
                    </div>
                    <div className="pros-icons col-lg-4">
                        <i className="fa fa-bell" aria-hidden="true"></i><span>10</span>
                    </div>
                </div>
                <ul className="nav navbar-nav navbar-right">
                    <li className="dropdown">
                    <a href="/" className="dropdown-toggle nav-profile-name" data-toggle="dropdown">Hi... Brret Lee <b className="caret"></b></a>
                    <ul className="dropdown-menu">
                      <li align="center" className="well">
                          <div><img className="img-responsive"  src="/images/user.png"alt="profile"/><a className="change" href="/">Change Picture</a></div>
                          <a href="/" className="btn btn-sm btn-default"><span className="glyphicon glyphicon-user"></span> MY Profile</a>
                          <a href="/" className="btn btn-sm btn-default"><span className="glyphicon glyphicon-log-out"></span> Logout</a>
                      </li>
                     </ul>
                  </li>
                </ul>
              </div>
            </div>
          </nav>
        );
    }
}
export default Header;